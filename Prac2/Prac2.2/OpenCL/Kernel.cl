
//TODO: set your arguments for the kernel. Note that you have to indicate if the argument is global or local. Global arguments are accessable by both host and this target device. While local can only be accessed by the device running this kernel. eg __global int* inputMatrixA, __local int* groupMemory

__kernel void matrixMultiplication(__global int* matrixA, __global int* matrixB, __global int* output){
	
	//TODO: program your kernel here
	//work items and work group numbers
	int workItemNum = get_global_id(0); //work item ID
	int workGroupNum = get_group_id(0); //work group ID	
	int localGroupID = get_local_id(0); //work items ID within each work group
	
	//memory buffers
	int mA = *matrixA;
	int mB = *matrixB;
	uint global_addr = workItemNum;
	
	//do matrix multiplication
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
		int sum = 0;
			for(int k=0; k<3; k++){
				sum = sum + mA*mB;
				//sum = sum + matrixA[k*3+i]*matrixB[j*3+k];
			}
		}
		output[j*3+i] = sum;	
		printf("Output:%i", output[i]);
	}
}




